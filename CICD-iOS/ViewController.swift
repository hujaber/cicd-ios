//
//  ViewController.swift
//  CICD-iOS
//
//  Created by Hussein Jaber on 09/03/2021.
//

import UIKit
import Calculate

class ViewController: UIViewController {

    private let calculator: Calculator = .init()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let x = calculator.multiply(1, 30)
        print(x)
    }


}

